# TODO

## Un seul script

Un seul script qui génère la requète SQL, appelle les exécutables, et extrait l'ID de media.


## Gestion des versions <= 7

Un seul script qui vérifie la version (cherche dans le registre) :

  * Si la version <= 7 :

    1. Lancer `omnidbutil -writedb -mmdb _repertoire d ecriture_`

    2. Extraire dans le fichier `stores.txt` la **première colonne** (l'ID de la library) correspondant au nom de library donner en argument (**deuxième colonne**), ainsi que la **quatrième colonne** (le nom du host)

    3. Extraire dans le fichier `cart.txt` la **septième colonne** (l'ID de media) lorsque la **deuxième colonne** correspond à l'ID de library.

  * Si la version > 7 :

    1. Reprendre le script préexistant


## Attaquer une library plutôt qu'un host et un store

A voir.
